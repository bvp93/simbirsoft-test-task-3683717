# Тестовое задание № 3683717

## Координаты

Бикеев Владислав +79271936874 vlad.bikeev.93@gmail.com

## Описание архитектуры системы

В системе пристутствуют 4  страницы: страница регистрации пользователя, страница с формой для ввода данных, страница с выводом данных, закрытая авторизацией и главная страница со ссылками на все остальные страницыю

Страницы:

http://localhost:8765 - Laravel app

http://localhost:15672 - RabbitMQ Manager(guest/guest)

Регистрация: http://localhost:8765/register

Авторизация: http://localhost:8765/login

Отпрвка сообщения в очередь rabbit: http://localhost:8765/rabbitmq-send

Получение новых сообщений из rabbit: http://localhost:8765/rabbitmq-receive

## Использованные внешние библиотеки и причины их выбора

https://github.com/php-amqplib/php-amqplib - для соединения с RabbitMQ

Можно было бы так же использовать библиотеку https://github.com/vyuldashev/laravel-queue-rabbitmq для соединения с RabbitMQ. Тут необходимо было бы делать через джобы. Если необходимо, могу и через джобы сделать.

## Инструкцию по запуску системы

```bash
1. git clone https://bvp93@bitbucket.org/bvp93/simbirsoft-test-task-3683717.git

2. cp .env.example .env - linux
   copy  .env.example .env - windows

3. docker-compose up -d

4. docker ps

5. composer install или docker-compose exec laravel-php composer install

6. docker-compose exec laravel-php php artisan key:generate

7. docker-compose exec laravel-php php artisan config:cache

8. docker-compose exec laravel-mysql bash

9. mysql -u root -p 

10. Enter password: 6Mfutn0UMF

11. GRANT ALL ON laravel_db.* TO 'laravel_user'@'%' IDENTIFIED BY '6Mfutn0UMF';

12. FLUSH PRIVILEGES;

13. EXIT;

14. exit

15. docker-compose exec laravel-php php artisan migrate
```

## Заложенное временя до старта работы

Задание получил давно, переоценил свое свободное время, не успел. Доделывал после 2 недельного отпуска, так как уезжал на юг.

## Затраченное время

Примерно 20 часов, по несколько часов в день, с перерывами.

Больше всего потратил на docker. Изучение и различные сборки.

Не удавлось сразу достаточно выделить время на выполнение тестового задания. Делал по чуть-чуть. 

В задании в пункте 2 ошибка скорее всего, как то неправильно сформулировано предложение.

