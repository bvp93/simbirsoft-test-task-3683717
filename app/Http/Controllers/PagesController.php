<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    /**
     * Show the application home page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('home');
    }

    /**
     * Show the application rabbitmg sending data form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sendMessageForm()
    {
        return view('rabbitmg.send-message-form');
    }
}
