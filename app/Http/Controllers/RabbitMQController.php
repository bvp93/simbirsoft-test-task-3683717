<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQController extends Controller
{
    /**
     * Connection with rabbit
     *
     * @var AMQPLazyConnection
     */
    private $connection;

    /**
     * Connection channel
     *
     * @var AMQPChannel
     */
    private $channel;

    /**
     * Queue name
     *
     * @var string
     */
    protected $queueName = 'messages';

    /**
     * Create a new controller instance.
     *
     * @param AMQPLazyConnection $connection
     * @return void
     */
    public function __construct(AMQPLazyConnection $connection)
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();
    }

    /**
     * Handle to send a new message to the queue
     *
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function send(Request $request)
    {
        $this->validator($request->all())->validate();

        $data = collect([
            'name' => trim($request->input('name')),
            'body' => trim($request->input('message')),
            'timestamp' => now()->timestamp
        ]);

        $this->channel->queue_declare(
            $this->queueName,
            false,
            false,
            false,
            false
        );

        $msg = new AMQPMessage($data->toJson());
        $this->channel->basic_publish($msg, '', $this->queueName);

        return redirect()->back()->withSuccess('Ваше сообщение отправлено.');
    }

    /**
     *  Handle to receive new messages from the queue
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function receive()
    {
        $messages = collect([]);

        list($queueName, $messageCount, $consumerCount) = $this->channel->queue_declare(
            $this->queueName,
            false,
            false,
            false,
            false
        );

        for ($i = 1; $i <= $messageCount; $i++) {
            if ($message = $this->channel->basic_get($this->queueName, true)) {
                $msg = \GuzzleHttp\json_decode($message->body);
                $msg->date = Carbon::createFromTimestamp($msg->timestamp);

                $messages->push($msg);
            }
        }

        return view('rabbitmg.receive-messages')->with([
            'messages' => $messages->sortByDesc('timestamp')
        ]);
    }

    /**
     * Get a validator for an incoming sent  message request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'message' => ['required', 'string', 'max:255']
        ]);
    }

    /**
     * Destruct controller instance
     *
     * @return void
     */
    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
