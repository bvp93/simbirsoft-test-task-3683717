<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Connection\AMQPLazyConnection;

class RabbitMQConnectionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $rabbitmqConfig = $this->app['config']['queue']['connections']['rabbitmq'];

        $this->app->bind(AMQPLazyConnection::class, function () use ($rabbitmqConfig) {
            return new AMQPLazyConnection(
                $rabbitmqConfig['host'],
                $rabbitmqConfig['port'],
                $rabbitmqConfig['user'],
                $rabbitmqConfig['password'],
                $rabbitmqConfig['vhost']
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
