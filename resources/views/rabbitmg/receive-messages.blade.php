@extends('layouts.app')

@section('title')
    {{ __('Новые сообщения') }}
@endsection

@section('content')
    <div class="container">
        @if ($messages->count() > 0)
            @foreach($messages as $message)
                <div class="alert alert-primary">
                    <p><strong>Имя: </strong>{{ $message->name }}</p>
                    <p><strong>Текст: </strong>{{ $message->body }}</p>
                    <p><strong>Дата и время: </strong>{{ $message->date->format('d.m.Y H:i:s') }}</p>
                </div>
            @endforeach
        @else
            <div class="alert alert-danger" role="alert">
                У вас нет новых сообщений в очереди!
            </div>
            @if(Route::has('rabbitmq-send'))
                <a href="{{ route('rabbitmq-send') }}" class="btn btn-primary">{{ __('Отправить') }}</a>
            @endif
        @endif
    </div>
@endsection
