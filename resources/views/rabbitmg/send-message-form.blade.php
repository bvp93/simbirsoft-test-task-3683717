@extends('layouts.app')

@section('title')
    {{ __('Отправка сообщения') }}
@endsection

@section('content')
    <form method="POST" action="{{ route('rabbitmq-send') }}">
        @csrf
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                       value="{{ request()->user() && !old('name') ? request()->user()->name : old('name') }}" required
                       autocomplete="name" autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="message" class="col-md-4 col-form-label text-md-right">
                {{ __('Текст сообщения') }}
            </label>

            <div class="col-md-6">
                <textarea class="form-control @error('message') is-invalid @enderror" name="message" id="message"
                          rows="3" required>{{ old('message') }}</textarea>

                @error('message')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">{{ __('Отправить') }}</button>
            </div>
        </div>
    </form>
@endsection
