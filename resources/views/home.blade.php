@extends('layouts.app')

@section('content')
    <h1 class="text-center">Тестовое задание</h1>
    <div class="mb-4">
        <p>
            В выполненном тестовом задании мы хотим увидеть веб-приложение, отвечающее
            следующим критериям:
        </p>
        <ul class="list-group">
            <li class="list-group-item ">
                1. Сделай четыре страницы: страница регистрации пользователя, страница с
                формой для ввода данных, страница с выводом данных, закрытая
                авторизацией и главная страница со ссылками на все остальные страницы,
                например в виде удобного меню
            </li>
            <li class="list-group-item">
                2. Кроме стандартного класса регистрации сделай класс с двумя методами
                (название страниц ты можешь сделать любыми, на твой вкус)
            </li>
            <li class="list-group-item">
                3. Логику методов мы видим такой: на одной странице будет форма отправки
                сообщений в RabbitMQ, который тебе придется так же настроить в Docker,
                на второй - вывод сообщений из очереди RabbitMQ на страницу
            </li>
            <li class="list-group-item">
                4. Оформляй клиент для отправки и получения сообщений из RabbitMQ в
                сервис-контейнер и подключай его через внедрение зависимостей
                (Dependency injection, DI). Если не знаешь, изучи, у нас такое придется
                использовать на каждом шагу
            </li>
            <li class="list-group-item">
                5. Закрой страницу вывода сообщений из RabbitMQ авторизацией
            </li>
            <li class="list-group-item">
                6. Форма с регистрацией должна единожды создавать только одного
                пользователя в бд, для проверки вывода сообщений. После регистрации
                первого пользователя, закрывай страницу регистрации от всех дальнейших
                переходов. У пользователя должен быть доступ к странице вывода.
            </li>
            <li class="list-group-item">
                7. Сделай все красиво и аккуратно, но без навороченного frontend`а
                (например, используй Twitter Bootstrap)
            </li>
        </ul>
        <p>
            Стек технологий для реализации веб-приложения (будет ооочень круто, если ты
            будешь использовать последние версии всех технологий)
        </p>
        <ul class="list-group list-group-flush">
            <li class="list-group-item list-group-item-primary">1. Laravel или Symfony</li>
            <li class="list-group-item list-group-item-primary">2. СУБД на выбор</li>
            <li class="list-group-item list-group-item-primary">3. Docker</li>
            <li class="list-group-item list-group-item-primary">4. RabbitMQ</li>
            <li class="list-group-item list-group-item-primary">5. Nginx</li>
            <li class="list-group-item list-group-item-primary">6. Любая операционная система внутри контейнера на твой
                выбор
            </li>
        </ul>
    </div>
    <div class="text-center">
        @if(Route::has('rabbitmq-send'))
            <a href="{{ route('rabbitmq-send') }}" class="btn btn-primary btn-large">Ввод данных</a>
        @endif
        @if(Route::has('rabbitmq-receive'))
            <a href="{{ 'rabbitmq-receive' }}" class="btn btn-primary btn-large">Вывод данных</a>
        @endif
    </div>
@endsection
